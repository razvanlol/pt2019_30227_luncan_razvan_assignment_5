package lambdaandstreaming;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class MonitoredData {

    private Date startTime;
    private Date endTime;
    private String activityName;
    private DateTime stTime;
    private DateTime enTime;
    private Duration duration;

    public MonitoredData(String startTime,String endTime,String activityName){
        String[] parts = startTime.split(" ");
        String part1 = parts[0];
        String part2 = parts[1];
        String[] days=part1.split("-");
        int yr=Integer.parseInt(days[0]);
        int mth=Integer.parseInt(days[1]);
        int dy=Integer.parseInt(days[2]);

        String[] times=part2.split(":");
        int hr=Integer.parseInt(times[0]);
        int min=Integer.parseInt(times[1]);
        int sec=Integer.parseInt(times[2]);

        String[] parts1 = endTime.split(" ");
        String part11 = parts1[0];
        String part21 = parts1[1];
        String[] days1=part11.split("-");
        int yr1=Integer.parseInt(days1[0]);
        int mth1=Integer.parseInt(days1[1]);
        int dy1=Integer.parseInt(days1[2]);

        String[] times1=part21.split(":");
        int hr1=Integer.parseInt(times1[0]);
        int min1=Integer.parseInt(times1[1]);
        int sec1=Integer.parseInt(times1[2]);

        this.stTime=new DateTime(yr,mth,dy,hr,min,sec);
        this.enTime=new DateTime(yr1,mth1,dy1,hr1,min1,sec1);
        this.duration=new Duration(stTime,enTime);

        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {

            this.startTime = sd.parse(startTime);
            this.endTime = sd.parse(endTime);
        }catch (ParseException e){
            System.out.println("Error");
        }
        this.activityName=activityName;
    }

    public String getDayMonthYearToStringStartTime(){
        Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("Europe/Osorhei"));
        calendar.setTime(startTime);
        return calendar.get(Calendar.YEAR)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
    }

    public String getDayMonthYearToStringEndTime(){
        Calendar calendar=Calendar.getInstance(TimeZone.getTimeZone("Europe/Osorhei"));
        calendar.setTime(endTime);
        return calendar.get(Calendar.YEAR)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
    }

    public String getActivityName() {
        return activityName;
    }

    public Long getDurationInSecods(){
        return this.duration.getStandardSeconds();
    }


    public Duration getDuration() {
        return duration;
    }

    public int getDay() {
        return this.stTime.getDayOfMonth();
    }

    public String toString(){
        return this.startTime+" "+this.endTime+" "+this.activityName;
    }



}
