package lambdaandstreaming;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.joda.time.Duration;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.util.stream.Collectors.summingInt;


public class Streaming {
    public void readFile(){
        String file = "Activities.txt";
        ArrayList<MonitoredData> monitoredData=new ArrayList<>();
        long task2=0;
        Map<String, Integer> task3 = new HashMap<>();
        ArrayList<String> dates=new ArrayList<>();
        List<Long> task5;
        Map<String,Map<String,Long>> task4;
        Map<String, Duration> task6;
        Map<String,List<Long>> task7;
        Map<String,Long> aux;
        Set<String> task7final;

        //Creating List Of Monitored Data
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            stream.map(line -> line.split("\\s{2,}"))
                    .map(a -> monitoredData.add(new MonitoredData(a[0], a[1], a[2])))
                    .collect(Collectors.toList());
            //Count How Many Times Each Activity Has Appeared Over The Entire Monitoring Period
            task3= monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityName, summingInt(x->1)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Task 1:");
        monitoredData.forEach(p->System.out.println(p));

        monitoredData.forEach(p->dates.add(p.getDayMonthYearToStringStartTime()));
        monitoredData.forEach(p->dates.add(p.getDayMonthYearToStringEndTime()));

        //Count How Many Days Of Monitored Data Appears In The Log
        System.out.println("Task 2:");
        task2=dates.stream().distinct().count();
        System.out.println(task2);

        System.out.println("Task 3:");
        task3.forEach((key,value)->System.out.println(key+" "+value));

        //Count How Many Times Each Activity Has Appeared For Each Day Over The Monitoring Period
        System.out.println("Task 4:");
        task4=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getDayMonthYearToStringStartTime,Collectors.groupingBy(MonitoredData::getActivityName,Collectors.counting())));
        task4.putAll(monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getDayMonthYearToStringEndTime,Collectors.groupingBy(MonitoredData::getActivityName,Collectors.counting()))));
        for(String integer:task4.keySet()){
            System.out.print(integer+": ");
            for(String str :task4.get(integer).keySet()){
                System.out.print(str+" "+task4.get(integer).get(str)+" ");
            }
            System.out.println();
        }

        //For Each Line Compute The Duration
        System.out.println("Task 5:");
        task5=monitoredData.stream().distinct().map(a->a.getDurationInSecods()).collect(Collectors.toList());
        task5.forEach(p->System.out.println(p));


        //For Each Activity Compute The Entire Duration Over The Montitoring Period
        System.out.println("Task 6:");
        task6=monitoredData.stream().collect(Collectors.toMap(a->a.getActivityName(),a->a.getDuration(),Duration::plus));
        task6.forEach((key,value)->System.out.println(key+" "+value.getStandardHours()+" "+value.getStandardMinutes()+" "+value.getStandardSeconds()));

        //Filter the activities that have 90% of the monitoring records with duration less than minutes
        System.out.println("Task 7:");

        aux=monitoredData.stream().filter(a->a.getDuration().getStandardMinutes()<5).collect(Collectors.groupingBy(MonitoredData::getActivityName,Collectors.counting()));
        System.out.println(aux.size());
        task7=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityName,Collectors.mapping(a->a.getDuration().getStandardMinutes(),Collectors.toList())));

        task7final=task7.entrySet().stream()
                .filter(x->getLessThanFive(x.getValue())-1<=Math.floor(x.getValue().size()*0.9) && getLessThanFive(x.getValue())+1>=Math.floor(x.getValue().size()*0.9))
                .collect(Collectors.toMap(x->x.getKey(),x->x.getValue())).keySet();
        task7final.forEach(p->System.out.println(p));
    }

    public static int getLessThanFive(List<Long> list){
        int nr=0;
        for(Long lng:list){
            if(lng<5){
                nr++;
            }
        }
        return nr;
    }
}
